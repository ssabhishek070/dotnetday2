﻿using ProductApp.product_model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.product_repository
{
    internal class ProductRepo
    {
        internal void getAllProduct(Product prod)
        {
            Console.WriteLine($" product id:: {prod.id}\t  product name :: {prod.name}\t product price :: {prod.price}");
        }

        internal void getProductName(Product prod)
        {
            Console.WriteLine($"product name:: {prod.name}"  );
        }
    }
}
