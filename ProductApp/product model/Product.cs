﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.product_model
{
    internal class Product
    {
        internal string id;
        internal string name;
        internal string price;

        public Product(string id,string name,string price)
        {
            this.id = id;
            this.name = name;
            this.price=price;
        }
    }
}
